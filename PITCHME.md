# Katuma

Una revisió tècnica per Panxacontenta

![Katuma](https://alpha.katuma.org/system/content_configurations/logos//original/ofn-logo-katuma.png)

---

## Katuma

- Contexte tècnic |
- Resultats equip avaluació |
- Mans a l'eina |

---

## Contexte tècnic

- Projecte amb tracció |
- Volum moderat, però creixent |
- Full de ruta ambiciós |
- Pla de viabilitat en definició |
  - Clau per la sostenibilitat i escalabilitat tècnica

---

### Xifres d'us

![Xifres](https://github.com/coopdevs/assemblea_katuma_30_05_2018/raw/master/images/daily_kpis.png)

Note:

* Actiu vol dir que ha provat almenys un cop el sistema, creant un cicle de
comanda
* Ho proven més productors que grups, en la línia de la investigació d'en Ricard

---

### Volum de comandes setmanals

**484.42€**
![Volum](https://github.com/coopdevs/assemblea_katuma_30_05_2018/raw/master/images/sum_item_total_by_week.png)

---

### Presència a Openfood Network

![Github](https://gitlab.com/eduponte/katuma-slides/raw/master/assets/images/Screen%20Shot%202018-06-15%20at%2010.58.56.png)

---

#### Grups en ús

* Cooperativa de Consum El Cabàs (Ca la Sisqueta)
* Faves Comptades
* Grup de consum La Panarra
* Panxacontenta
* Grup de Consum El Cabàs (Ateneu)
* Ridorta.Grup de consum ecològic d'Horta
* La bastida
* A Gradicela
* R.C.V. Central de Abastecimiento
* Horta da Partilha

Note:
* A l'hora de l'ús habitual hi ha més grups que productors, perquè el productors
    intenten que tot els seus grups facin servir Katuma. És el cas de Pagerols
    i Conreu Sereny.

---

#### Productores en ús

* L'Estol ecològic
* Pagerols agricultura b.i.o...agradable
* Conreu Sereny
* L'Aresta Cooperativa
* A Gradicela
* Arqueixal
* Horta da Partilha
* R.C.V. Central de Abastecimiento

---

### Estat d'Open Food Network

* Únic equip de desenvolupament
* Millora en l'organització de la feina
* Versió nova cada dues setmanes
* Estandarització de la infraestructura
* Sessions de treball de futures funcionalitats
* OFN Estats Units i OFN Alemanya iniciant-se
* Contactes amb possibles OFN Bèlgica i OFN Itàlia

---

## Resultats equip avaluació

* Pros
* Contres

_L'equip: mongetes, farigoles, espàrrecs i olives_

---

### Pros: Operativa

* interfaç agradable, amb la composició de la cistella setmanal |
* fer la comanda és més fàcil gràcies als filtres |
* s'envia un email-resum de la comanda |
* es poden afegir comentaris a la comanda |

---

### Pros: Operativa

* les quantitats són camps predefinits |
  * evita quantitats rares |
  * evita confusió de "." i "," |
* prevé esborrat accidental del full de comanda (total o parcial) |
* diversitat d'informes d'administració |

---

### Pros: OFN

* participació de katuma dins openfoodnetwork: coopdevs tiren del carro i són capaços d'evolucionar en el sentit desitjat |
* malgrat estar basada en un framework d'ecommerce, majoria d'implantacions són adreçades a grups de consum |
* baix risc d'esdevenir un marketplace, a dia d'avui |
  * un hipotètic marketplace segurament hauria de triar una altra eina, ja que aquesta haurà evolucionat en la direcció oposada |

---

### Pros: Estoc

* gestió de l'estoc mitjançant la funcionalitat d'inventari |
  * tot i que aquesta funcionalitat s'ha de millorar :) |
* opció alternativa: proveïdor virtual |

---

### Pros: Més enllà de les funcionalitats

* Pot esdevenir una eina potent de sensibilització i aprenentatge interns en tecnologies lliures |

---

### Cons: Operativa

* dificultats esporàdiques per registrar-se |
* és obligatori identificar-se: nom, lloc, tel |
* només els administradors del grup poden editar una comanda validada, tot i estar dins del termini de comandes |
* no s'està indicant procedència dels productes (Pagerols) |
* manca de solucions per gestió de les incidències durant la permanència |

Note:

Considerem que Katuma pot generar sense problema una matriu resum de la comanda de totes les unitats, per un productor donat. Aquests taula, que s'hauria d'imprimir i portar a la permanència, no és modificable. Per tant, si hi ha un desquadre entre lo que arriba a la permanència i lo que s'ha demanat, la gent que fa la permanència hauria de fer els càlculs a mà per tornar a calcular la despesa global de la/les unitats afectades. No sé si m'explico. Exemple: si 10 unitats han demanat taronjes i que al final no arriben les taronjes, llavors s'ha de calcular i restar a mà a aquestes 10 unitats el preu de les taronjes, quan ara mateix, borrem la columna i el càlcul de despesa s'actualitza tot solet.
De fet, per agilitzar s'hauria de poder generar la taula-resum setmanal en format full de càlcul ods o xls. no sé si això es pot plantejar als desenvolupadors/res del Katuma

---

### Cons: Operativa

* pèrdua d'autonomia: pel que fa a la gestió de les incidències en la plataforma, som depenent dels administradors/tècnics |
* possibles períodes d'indisponibilitat del sistema, o parts del mateix (informes) |
* infrastructures fràgils a dia d'avui |

---

### Cons: Senyals d'alerta

* manca d'ample de banda a l'equip de desenvolupament |
  * per mitigar aquest risc, caldria una implicació més activa a katuma que ens donés autonomia |
* l'eina té força deute tècnic de les seves primeres versions (p.e. versió de spree) |

---?image=https://alpha.katuma.org/assets/home/home.jpg
@title[Katuma]

## Mans a l'eina

https://alpha.katuma.org/

<em style="font-size: 20px">
[_Informe d'entrega_](https://gitlab.com/eduponte/katuma-slides/blob/master/assets/pdf/informe_panxacontenta.pdf)
</em>

---?image=https://alpha.katuma.org/assets/home/home.jpg
@title[Katuma]

## Gràcies!


![YouTube Video](https://www.youtube.com/embed/7s4PdyWt7ZY)
